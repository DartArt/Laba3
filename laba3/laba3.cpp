#include "stdafx.h"
#include <stdio.h>
#include "mpi.h"
#include <math.h>


int main(int  argc, char * argv[])
{
	double PI25DT = 3.141592653589793238462643;		// эталон
	int rank, size, resultlen, n, i;
	double startwtime = 0.0, endwtime, sum, x, mypi, h;
	char name[MPI_MAX_PROCESSOR_NAME];
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Get_processor_name(name, &resultlen);
	printf("Процесс номер %d запущен на компьютере: '%s'\n", rank, name);
	fflush(stdout);
	MPI_Barrier(MPI_COMM_WORLD);
	do {
		if (rank == 0) {
			printf("Введите количество интервалов (n), (0 для выхода): ");
			fflush(stdout);
			scanf("%d", &n);
		}
		startwtime = MPI_Wtime();
		MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
		sum = 0.0; h = 2 * n; i = rank + 1;
		if (n <= 0) break;
		while (i <= n) {
			x = (2 * i - 1) / h;
			sum += 4 / (1 + x * x);
			i += size;
		}
		sum *= 1 / (double)n;
		MPI_Reduce(&sum, &mypi, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
		endwtime = MPI_Wtime();
		if (rank == 0) {
			printf("результат: pi=%.16f, Расхождение с эталоном %.16f\nвремя работы %f секунд\n", mypi, fabs(mypi - PI25DT), endwtime - startwtime);
			fflush(stdout);
		}
	} while (n > 0);
	MPI_Finalize();
	return 0;
}
